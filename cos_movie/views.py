__author__ = "Salimcan Karadeniz"
__email__ = "salimcankaradeniz@gmail.com"

import json
import requests

from json import JSONDecodeError
from operator import itemgetter
from itertools import groupby, combinations
from functools import reduce

from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.urls import reverse
from django.views import generic
from rest_framework.response import Response
from rest_framework.views import APIView

from movie_django import settings


class HomeRedirectView(generic.RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.is_mobile:
            return reverse('mobile_home')
        else:
            return reverse('desktop_home')


class DesktopHomeView(generic.TemplateView):
    template_name = "{}/desktop/base.html".format(settings.THEME_NAME)


class MobileHomeView(generic.TemplateView):
    template_name = "{}/mobile/base.html".format(settings.THEME_NAME)


class MovieAPIView(APIView):
    endpoint_url = None
    token = None
    cache_key = None
    is_cached = False
    cache_timeout = 86400
    params = None

    def set_params(self, body):
        try:
            self.params = json.loads(body.decode())
        except AttributeError:
            self.params = {}
        except JSONDecodeError:
            self.params = {}

    def request_to_api(self, method):
        if self.token is None:
            return Response(status=403)

        headers = {
            'Authorization': '{}'.format(self.token),
            'Content-Type': 'application/json'
        }

        if self.is_cached and self.cache_key is not None:
            data = cache.get(self.cache_key)
            if data is not None:
                return Response(data)

        if self.params is None:
            response = requests.request(
                method,
                self.endpoint_url,
                headers=headers
            )
        else:
            response = requests.request(
                method,
                self.endpoint_url,
                headers=headers,
                json=self.params
            )

        if response.status_code not in [200, 201, ]:
            try:
                return Response(response.json(),
                                status=response.status_code)
            except TypeError:
                pass
            except ValueError:
                pass

            try:
                return Response(
                    {"error": json.loads(response.content.decode())},
                    status=response.status_code)
            except JSONDecodeError:
                return Response(
                    {"error": response.content.decode()},
                    status=response.status_code)

        try:
            response_data = response.json()
        except TypeError:
            return Response({"error": json.loads(response.content.decode())},
                            status=response.status_code)

        if 'data' in response_data:
            return_data = response_data['data']
        else:
            return_data = response_data

        if self.is_cached and self.cache_key is not None:
            cache.set(self.cache_key, return_data,
                      timeout=self.cache_timeout)

        return Response(return_data)

    def get(self, request, *args, **kwargs):
        return self.request_to_api('GET')

    def post(self, request, *args, **kwargs):
        return self.request_to_api('POST')

    def patch(self, request, *args, **kwargs):
        return self.request_to_api('PATCH')


class CategoryListAPIView(MovieAPIView):
    http_method_names = ['get', ]

    def dispatch(self, request, *args, **kwargs):
        self.endpoint_url = settings.API_ENDPOINTS['Category']['LIST']['URL']
        self.token = settings.TOKENS['API']
        self.is_cached = True
        self.cache_timeout = 60 * 60 * 24 * 365
        self.cache_key = 'categories'

        return super().dispatch(request, *args, **kwargs)


class MovieListAPIView(MovieAPIView):
    http_method_names = ['get', ]

    def dispatch(self, request, *args, **kwargs):
        self.endpoint_url = settings.API_ENDPOINTS['Movie']['LIST']['URL']
        self.token = settings.TOKENS['API']
        self.is_cached = True
        self.cache_timeout = 60 * 60 * 24 * 365
        self.cache_key = 'movies'

        return super().dispatch(request, *args, **kwargs)


class MovieCategoryListView(MovieAPIView):
    http_method_names = ['post', ]

    def dispatch(self, request, *args, **kwargs):
        endpoint = settings.API_ENDPOINTS['Movie']['MOVIE_CATEGORY_LIST']
        self.endpoint_url = endpoint['URL']
        self.token = settings.TOKENS['API']
        self.params = json.loads(request.body.decode())

        return super().dispatch(request, *args, **kwargs)


class MovieSingleView(MovieAPIView):
    http_method_names = ['post', ]

    def dispatch(self, request, *args, **kwargs):
        endpoint = settings.API_ENDPOINTS["Movie"]["MOVIE_SINGLE"]
        self.endpoint_url = endpoint["URL"]
        self.token = settings.TOKENS["API"]
        self.params = json.loads(request.body.decode())

        return super().dispatch(request, *args, **kwargs)


class MovieNameFilterView(MovieAPIView):
    http_method_names = ['post', ]

    def dispatch(self, request, *args, **kwargs):
        endpoint = settings.API_ENDPOINTS["Movie"]["MOVIE_NAME"]
        self.endpoint_url = endpoint["URL"]
        self.token = settings.TOKENS["API"]
        self.params = json.loads(request.body.decode())

        return super().dispatch(request, *args, **kwargs)


class MovieImdbFilterView(MovieAPIView):
    http_method_names = ['post', ]

    def dispatch(self, request, *args, **kwargs):
        endpoint = settings.API_ENDPOINTS["Movie"]["MOVIE_IMDB"]
        self.endpoint_url = endpoint["URL"]
        self.token = settings.TOKENS["API"]
        self.params = json.loads(request.body.decode())

        return super().dispatch(request, *args, **kwargs)


class MovieCategoryTop10ListView(MovieAPIView):
    http_method_names = ["get", ]

    def dispatch(self, request, *args, **kwargs):
        self.endpoint_url = settings.API_ENDPOINTS['Movie']['MOVIE_TOP10']['URL']
        self.token = settings.TOKENS['API']
        self.is_cached = True
        self.cache_timeout = 60 * 60 * 24 * 365
        self.cache_key = 'movie_top10'

        return super().dispatch(request, *args, **kwargs)
